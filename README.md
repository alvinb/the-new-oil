# The New Oil
<img src="https://img.shields.io/liberapay/receives/thenewoil.svg?logo=liberapay">

The New Oil is a data privacy and cybersecurity website dedicated to helping newbies take their first steps into the subject matter.

## Contributing

Technology is a constantly changing field, and as people who take their privacy and security seriously, we place even more trust in the information on this site than most other technologies. As such, it is vital that The New Oil stays as current as possible. All readers are welcome to submit updates and suggested changes via a new [issue](https://gitlab.com/nbartram/the-new-oil/-/issues).

Readers are also encouraged to join the [Matrix](https://riot.im/app/#/room/#TheNewOil:matrix.org) room for general discussion and meeting other like-minded privacy enthusiasts.

## Other Online Presences

- [Website](https://thenewoil.xyz/)
- [Blog](https://write.as/thenewoil/)
- [Podcast](https://techlore.tech/sr.html)
- [Mastodon](https://freeradical.zone/@thenewoil)
- [PeerTube](https://peertube.thenewoil.xyz/video-channels/thenewoil/videos)

## Support

In addition to submitting corrections and changes, there are several ways for users to support The New Oil.

- [Bitcoin](https://thenewoil.xyz/btc.html): xpub661MyMwAqRbcGzNpy8KujhemxoFES1n7Sbc4JsxsJpCByqFGRVtxMXWpcLV318wxPj1YfuUeGbh2zWnLicad62pVKqXJbwMDorMt7vGV3gW
- [Monero](https://thenewoil.xyz/xmr.html): 44NAYMG1qupZcZ5WtjHWp58WNkXjetpTbRKHkVyrFZs8SiZK7SycQKdAm5y7sXuVhV1eYcShLjkKpbLRQSZaKxpvV2zo8ii
- [ZCash](https://thenewoil.xyz/zec.html): t1PSozRb2qv92N6TaPkYS4GJdEwk9Bo7WqD
- [Liberapay](https://liberapay.com/thenewoil)
- [Patreon](https://www.patreon.com/thenewoil)
- [PayPal](https://www.paypal.com/donate?token=bsoRAryUMp9I3oU2oJeHIxCkMmXXjKjMUEpQgZZ1Eyuv5JKU_oMJHrhyH04rYD4HRPW7f8MQr619hH4v): thenewoil@protonmail.com
- Spreading the word and sharing the site with your friends and family!

**[2020 Transparency Report](https://write.as/thenewoil/2020-recap-2021-plans)**
